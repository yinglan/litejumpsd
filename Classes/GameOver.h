//
//  GameOver.h
//  FatJumper
//
//  Created by in-blue  on 10-10-29.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameScene.h"
#import "MainScene.h"
 
@interface GameOver : CCLayer
{
	BOOL m_endless;
	int m_state;
	int m_score;
	int m_num;
	int m_totalnum;
	int m_allcoins;
	int m_player;
	int m_hamburg;
	CCMenuItem *retry;
	CCMenuItem *exit;
	CCMenuItem *open;
	CCLabelAtlas *hightLabel;//高度记录
	CCLabelAtlas *numLabel;//金币计数
	int getscore;
	int getnum;
	CCMenuItem*		m_pBtnGameCenter;
	CCSprite*		m_pGameCenterBack;
	CCMenuItem*		m_pBtnLeaderBoard;
	CCMenuItem*		m_pBtnAchievment;
}
+(id) scene;
- (id)initWithdata:(BOOL)endless:(int)player:(int)state:(int)Lastscore:(int)Lastnum:(int)LastHamburg;
- (void)loadNum;
- (void)saveNum;
- (void)step:(ccTime)dt;
- (void)sumbitHighScore:(BOOL)endless;
-(void)initSound;
+(GameOver *) sendToter;
- (void)exitData;
@end
