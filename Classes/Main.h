//
//  Main.h
//  FatJumper
//
//  Created by in-blue  on 11-2-11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Constant.h"
#import "SimpleAudioEngine.h"
@interface Main : CCLayer

{
	CCNode *p_node_0;
	CCNode *p_node_1;
	
	NSMutableArray *p_node0_array;
	NSMutableArray *p_node1_array;
	NSMutableArray *p_node2_array;
	CCNode *p_mapNode_BG;
	CCNode *p_mapNode_MD;
	CCNode *p_mapNode_FR;
	CCNode *p_mapNode_LT;
	CCNode *p_mapNode_LT2;
	CCSprite *_BG;
	CCSprite *_MD;
	CCSprite *_FR;
	CCSprite *_LT;
	CCSprite *_LT2;
	CCSprite *_LT3;
	NSMutableArray *p_array_BG;
	NSMutableArray *p_array_MD;
	NSMutableArray *p_array_FR;
	NSMutableArray *p_array_LT;
	
	int m_callbackCount_BG;
	int m_mapCurrentIndex_BG;
	
	int m_mapCurrentIndex_MD;
	
	int m_mapCurrentIndex_FR;
	
	NSMutableArray *p_block0_array;
	NSMutableArray *p_block1_array;
	
	CCSpeed *p_BGSpeed;
	CCSpeed *p_MDSpeed;
	CCSpeed *p_FRSpeed;
	CCSpeed *p_LTSpeed;
	CCSpeed *p_LT2Speed;
	
	BOOL willstop;
	int m_state;
	int kk;
	int m_Itemjudge;
	int rocketx;
	
	BOOL gameEndless;
	
	// 小新
	int  m_nGhostEyeCount;
}
- (void)initMap;





- (void)rocket5:(int)count:(int)judge;
- (void)rocket6:(int)count:(int)judge;


- (void)rocket7:(int)count:(int)judge;
- (void)rocket8:(int)count:(int)judge;

- (void)rocket9:(int)count:(int)judge;
- (void)rocket10:(int)count:(int)judge;


- (void)rocket11:(int)count:(int)judge;
- (void)rocket12:(int)count:(int)judge;
- (void)rocket13:(int)count:(int)judge;
- (void)rocket14:(int)count:(int)judge;

- (void)rocket15:(int)count:(int)judge;
- (void)rocket16:(int)count:(int)judge;
- (void)rocket17:(int)count:(int)judge;
- (void)rocket18:(int)count:(int)judge;

- (void)rocket19:(int)count:(int)judge;
- (void)rocket20:(int)count:(int)judge;
- (void)rocket21:(int)count:(int)judge;

- (void)rocket22:(int)count:(int)judge;
- (void)rocket23:(int)count:(int)judge;
- (void)rocket24:(int)count:(int)judge;
- (void)rocket25:(int)count:(int)judge;

- (void)rocket26:(int)count:(int)judge;
- (void)rocket27:(int)count:(int)judge;

- (void)rocket28:(int)count:(int)judge;

- (void)rocket29:(int)count:(int)judge;
- (void)rocket30:(int)count:(int)judge;

- (void)rocket31:(int)count:(int)judge;
- (void)gameBreakRocket:(int)count;

- (void)clouds1:(int)count;
- (void)clouds2:(int)count;

- (void)clouds3:(int)count;
- (void)clouds4:(int)count;


- (void)clouds6:(int)count;
- (void)clouds7:(int)count;
- (void)clouds8:(int)count;


- (void)clouds9:(int)count;
- (void)clouds10:(int)count;
- (void)clouds11:(int)count;



- (void)clouds12:(int)count;
- (void)clouds13:(int)count;


- (void)clouds15:(int)count;
- (void)clouds16:(int)count;

- (void)clouds17:(int)count;
- (void)clouds18:(int)count;

- (void)clouds19:(int)count;
- (void)clouds20:(int)count;

- (void)clouds21:(int)count;
- (void)clouds22:(int)count;

- (void)clouds23:(int)count;
- (void)clouds24:(int)count;
- (void)clouds25:(int)count;

- (void)A1:(int)count;
- (void)A2:(int)count;

- (void)A3:(int)count;
- (void)A4:(int)count;

- (void)A5:(int)count;
- (void)A6:(int)count;

- (void)A7:(int)count;
- (void)A8:(int)count;

- (void)A9:(int)count;
- (void)A10:(int)count;

- (void)A11:(int)count;
- (void)A12:(int)count;


- (void)A13:(int)count;
- (void)A14:(int)count;
- (void)A15:(int)count;
- (void)A16:(int)count;

- (void)A17:(int)count;
- (void)A18:(int)count;
- (void)A19:(int)count;
- (void)A20:(int)count;


- (void)ABlock1:(int)count;

- (void)ABlock2:(int)count;
- (void)ABlock3:(int)count;
- (void)ABlock4:(int)count;
- (void)ABlock5:(int)count;

- (void)ABlock6:(int)count;
- (void)ABlock7:(int)count;

- (void)ABlock8:(int)count;
- (void)ABlock9:(int)count;



- (void)ABlock12:(int)count;
- (void)ABlock13:(int)count;

- (void)ABlock14:(int)count;
- (void)ABlock15:(int)count;

- (void)ABlock16:(int)count;
- (void)ABlock17:(int)count;

- (void)ABlock18:(int)count;


- (void)ABlock21:(int)count;
- (void)ABlock22:(int)count;

- (void)ABlock23:(int)count;
- (void)ABlock24:(int)count;







- (void)initbigNormalmuch:(int)count:(int)height:(int)state;
- (void)initbigNormalless:(int)count:(int)height:(int)state;

- (void)initsmallNormalmuch:(int)count:(int)height:(int)state;
- (void)initsmallNormalless:(int)count:(int)height:(int)state;


- (void)initmixedNormalmuch:(int)count:(int)height:(int)state;
- (void)initmixedNormalless:(int)count:(int)height:(int)state;

- (void)initmovedNormabig:(int)count:(int)state;
- (void)initmoveNormalsmall:(int)count:(int)state;


- (void)initShape1:(int)count;
- (void)initShape2:(int)count;
- (void)initShape3:(int)count;
- (void)initShape4:(int)count;
- (void)initShape5:(int)count;
- (void)initShape6:(int)count;
- (void)initShape7:(int)count;
- (void)initShape8:(int)count;
- (void)initShape9:(int)count;
- (void)initShape10:(int)count;
- (void)initShape11:(int)count;
- (void)initShape12:(int)count;

- (void)Type1:(int)count:(int)height;
- (void)Type2:(int)count:(int)height;
- (void)Type3:(int)count:(int)height;
- (void)Type4:(int)count:(int)height;
- (void)Type5:(int)count:(int)height;
- (void)Type6:(int)count:(int)height;
- (void)Type7:(int)count:(int)height;


- (void)Type8:(int)count;
- (void)Type9:(int)count;
- (void)Type10:(int)count;
- (void)Type11:(int)count;
- (void)Type12:(int)count;
- (void)Type13:(int)count:(int)state;

- (void)Type14:(int)count:(int)state;
- (void)Type15:(int)count;
- (void)Type16:(int)count;
- (void)Type17:(int)count:(int)state;
- (void)Type18:(int)count;

- (void)Type19:(int)count;

- (void)Type22:(int)count;


- (void)Type23:(int)count:(int)state;
- (void)Type24:(int)count:(int)state;

- (void)Type25:(int)count;
- (void)Type26:(int)count;


- (void)Blink3:(int)count;

- (void)Blink4:(int)count;
- (void)Blink5:(int)count;



- (void)Blink10:(int)count;
- (void)Blink11:(int)count;

- (void)Blink12:(int)count;
- (void)Blink13:(int)count;

- (void)Blink14:(int)count;

- (void)Blink15:(int)count;
- (void)Blink16:(int)count;

- (void)Blink17:(int)count;


- (void)Ghost1:(int)count;
- (void)Ghost2:(int)count;

- (void)Ghost3:(int)count;
- (void)Ghost4:(int)count;

- (void)Ghost5:(int)count;

- (void)Ghost6:(int)count;



- (void)Fun1:(int)count;

- (void)Fun2:(int)count;
- (void)Fun3:(int)count;
- (void)Fun4:(int)count;
- (void)Fun5:(int)count;

- (void)Fun6:(int)count;
- (void)Fun7:(int)count;
- (void)Fun8:(int)count;

- (void)Fun9:(int)count;

@end
