//
//  PauseScene.m
//  PauseTest
//
//  Created by piepie on 11-4-30.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PauseScene.h"


@implementation PauseScene



+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	PauseScene *layer = [PauseScene node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init] ))
	{
		

		p_pauseBoard = [CCSprite spriteWithFile:@"pause_board.png"];
		[self addChild:p_pauseBoard z:3];
		p_pauseBoard.position = ccp(160,240);
	
		
		
		item_resumegame = [CCMenuItemImage itemFromNormalImage:@"resume_button.png"
												 selectedImage:@"resume_button_down.png"
														target:self
													  selector:@selector(resumeFunc:)];
		item_retry = [CCMenuItemImage itemFromNormalImage:@"restart_button.png"
											selectedImage:@"restart_button_down.png"
												   target:self
												 selector:@selector(retryFunc:)];
		item_exit = [CCMenuItemImage itemFromNormalImage:@"quit_button.png"
										   selectedImage:@"quit_button_down.png"
												  target:self
												selector:@selector(exitFunc:)];
		
		item_resumegame.position = ccp(42,77);               
		item_retry.position = ccp(37,23);                   
		item_exit.position = ccp(17,-31);   
		

		
		CCMenu *p_menu2 = [CCMenu menuWithItems: item_resumegame,item_retry,item_exit,nil];
		[self addChild:p_menu2 z:3];
		[self initSound];
		
	}
	return self;
}


-(void)initSound
{
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Cancel.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Confirm.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Cancel.aif"];

}

#pragma mark -
- (void)exitFunc: (id)sender
{
	
	[[CCDirector sharedDirector] resume];
	[[SimpleAudioEngine sharedEngine] playEffect:@"Cancel.aif"];
	[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
	[self unscheduleAllSelectors];
	[self removeAllChildrenWithCleanup:YES];
	[[CCDirector sharedDirector] replaceScene: [MainScene scene]];
	
}
#pragma mark -
- (void)resumeFunc: (id)sender
{
	[[SimpleAudioEngine sharedEngine] playEffect:@"Confirm.aif"];
	[[CCDirector sharedDirector] popScene];

	
}
#pragma mark -
- (void)retryFunc: (id)sender
{
	[[SimpleAudioEngine sharedEngine] playEffect:@"Cancel.aif"];
	[[CCDirector sharedDirector] resume];
	[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
	[self unscheduleAllSelectors];
	[self removeAllChildrenWithCleanup:YES];
	[[CCDirector sharedDirector] replaceScene: [GameScene scene]];
	
}

@end
