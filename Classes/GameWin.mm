//
//  Gamewin.mm
//  FatJumper
//
//  Created by jacko on 11-2-14.
//  Copyright 2011 Inblue.co.ltd. All rights reserved.
//

#import "GameWin.h"


@implementation GameWin
#pragma mark -
+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GameWin *layer = [GameWin node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	
	
	// return the scene
	return scene;
}
#pragma mark -
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init] )) 
	{
		int player = [[NSUserDefaults standardUserDefaults] integerForKey: @"playerKey"];
		BOOL endless = [[NSUserDefaults standardUserDefaults] integerForKey:@"GameEndless"];
		int laststate = [[NSUserDefaults standardUserDefaults] integerForKey:@"GameState"];
		int lastscore = [[NSUserDefaults standardUserDefaults] integerForKey:@"GameScore"];
		int lastnum = [[NSUserDefaults standardUserDefaults] integerForKey:@"Gamemoney"];
		int LastHamburg  = [[NSUserDefaults standardUserDefaults] integerForKey:@"GameHamburg"];
		[self initWithdata:endless :player :laststate :lastscore :lastnum:LastHamburg];
	}
	return self;
}
#pragma mark -
- (id)initWithdata:(BOOL)endless:(int)player:(int)state:(int)Lastscore:(int)Lastnum:(int)LastHamburg
{	
	self.isTouchEnabled = YES; 	
	m_endless = endless;
	m_player = player;
	m_state = state;

	m_num = Lastnum;
	m_player = player;
	m_hamburg = LastHamburg;
	
	
	m_num += (int)m_hamburg/20;
	getnum = 0;
	falldone = NO;
	[self initSound];	
	
	[self loadNum];
	[self saveNum];
	
	[self initItem];
	
	CCSprite *p_bg = [CCSprite spriteWithFile:@"GameWinboard.png"];
	[self addChild:p_bg z:0];
	p_bg.position = ccp(160,240);
	
	
	return self;
}
-(void)initSound
{
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Unlock.aif"];
	[[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"GameWin1.aif"]; 
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Cancel.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Confirm.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Replacescene.aif"];

}
#pragma mark -
-(void)initItem
{

	
	switch (m_state)
	{
			
		case 3:
		{
			if (m_unlock1_first == NO)
			{
				m_unlock1_first = YES;
				[[NSUserDefaults standardUserDefaults] setBool:m_unlock1_first forKey:@"unlock1_first"];
				
				CCSprite *p_bg = [CCSprite spriteWithFile:@"stageunlock_1.jpg"];
				[self addChild:p_bg z:1];
				p_bg.position = ccp(160,240);
				
				blur1 = [CCSprite spriteWithFile:@"stageunlock_1_blur.jpg"];
				[self addChild:blur1 z:2];
				blur1.position = ccp(160,240);
				blur1.visible = NO;
				
				m_unlock = [[NSUserDefaults standardUserDefaults] boolForKey:@"UnstatelockKey1"];
				m_unlock = NO;
				[[NSUserDefaults standardUserDefaults] setBool:m_unlock forKey:@"UnstatelockKey1"];
				[[SimpleAudioEngine sharedEngine] playEffect:@"Unlock.aif"];
				
 			}
		}
			break;

		case 6:
		{
			if (m_unlock2_first == NO)
			{
				m_unlock2_first = YES;
				[[NSUserDefaults standardUserDefaults] setBool:m_unlock2_first forKey:@"unlock2_first"];
				
				CCSprite *p_bg = [CCSprite spriteWithFile:@"stageunlock_2.jpg"];
				[self addChild:p_bg z:1];
				p_bg.position = ccp(160,240);
				
				
				
				blur2 = [CCSprite spriteWithFile:@"stageunlock_2_blur.jpg"];
				[self addChild:blur2 z:2];
				blur2.position = ccp(160,240);
				blur2.visible = NO;
				
				
				m_unlock = [[NSUserDefaults standardUserDefaults] boolForKey:@"UnstatelockKey2"];
				m_unlock = NO;
				[[NSUserDefaults standardUserDefaults] setBool:m_unlock forKey:@"UnstatelockKey2"];
				[[SimpleAudioEngine sharedEngine] playEffect:@"Unlock.aif"];
				
 			}

		}
			break;

	
		case 9:
		{
			if (m_unlock3_first == NO)
			{
				m_unlock3_first = YES;
				[[NSUserDefaults standardUserDefaults] setBool:m_unlock3_first forKey:@"unlock3_first"];
				
				CCSprite *p_bg = [CCSprite spriteWithFile:@"stageunlock_3.jpg"];
				[self addChild:p_bg z:1];
				p_bg.position = ccp(160,240);
				
				
				blur3 = [CCSprite spriteWithFile:@"stageunlock_3_blur.jpg"];
				[self addChild:blur3 z:2];
				blur3.position = ccp(160,240);
				blur3.visible = NO;
				
				m_unlock = [[NSUserDefaults standardUserDefaults] boolForKey:@"UnstatelockKey3"];
				m_unlock = NO;
				[[NSUserDefaults standardUserDefaults] setBool:m_unlock forKey:@"UnstatelockKey3"];
				[[SimpleAudioEngine sharedEngine] playEffect:@"Unlock.aif"];
				
 			}
			

		}
			break;
		default:
			break;
	}
	
	
	p_bg2 = [CCSprite spriteWithFile:@"gamewin_bg_board.png"];
	[self addChild:p_bg2 z:3];
	p_bg2.position = ccp(160,720);
	
	if (m_state !=9)
	{
		next = [CCMenuItemImage itemFromNormalImage:@"next_button.png"
									  selectedImage:@"next_button_down.png"
											 target:self
										   selector:@selector(nextFunc:)];
	}
	
	else
	{
		next = [CCMenuItemImage itemFromNormalImage:@"next_button_gray.png"
									  selectedImage:@"next_button_gray_down.png"
											 target:self
										   selector:@selector(nextFunc:)];
		
	}

	
		exit = [CCMenuItemImage itemFromNormalImage:@"quit_button.png"
								  selectedImage:@"quit_button_down.png"
										 target:self
									   selector:@selector(exitFunc:)];
	
	next.position = ccp(20,-100);
	[next setIsEnabled:NO];
	exit.position = ccp(20,-160);
	[exit setIsEnabled:NO];
	CCMenu *menu = [CCMenu menuWithItems:next,exit,nil];
	[p_bg2 addChild:menu z:2];
	
	
	
	numLabel = [CCLabelAtlas labelWithString:@"000" charMapFile:@"coin_number_big.png" itemWidth:22 itemHeight:57 startCharMap:'.'];
	[self addChild:numLabel z:3 tag:kNumLabel];
	numLabel.position = ccp(175,250);
	numLabel.visible = NO;
	
	if (m_state != 3 && m_state !=6 && m_state !=9)
	{
		[self touchtogo];
	}
}

#pragma mark -
- (void)touchtogo
{
	[p_bg2 runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.05 position:ccp(160,220)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-22)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,58)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-29)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,21)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-9)],[CCCallFuncN actionWithTarget:self selector:@selector(actionDone:)],nil]];

	switch (m_state)
	{
			
		case 3:
		{
			blur1.visible = YES;
			
		}
			break;
			
		case 6:
		{
			blur2.visible = YES;
			
			
		}
			break;
			
			
		case 9:
		{
			blur3.visible = YES;
			
			
			
		}
			break;
		default:
			break;
	}
}

#pragma mark -
- (void)step:(ccTime)dt

{
	if (getnum < m_num)
	{
		getnum++;
	}
	
	NSString *p_hightscoreStr = [NSString stringWithFormat:@"%03d",getnum];
	numLabel = (CCLabelAtlas *)[self getChildByTag:kNumLabel];
	[numLabel setString:p_hightscoreStr];

	
}

#pragma mark -
- (void)saveNum
{
	m_totalnum += m_num;
	[[NSUserDefaults standardUserDefaults] setInteger:m_totalnum forKey: @"totalNum"];
	m_allcions += m_num;
}
#pragma mark -
- (void)loadNum
{
	
	m_totalnum = [[NSUserDefaults standardUserDefaults] integerForKey: @"totalNum"];
	
}

- (void)actionDone:(id)sender
{
	if (m_state == 9)
	{
		[next setIsEnabled:NO];
	}
	else 
	{
		[next setIsEnabled:YES];
	}

	
	
	[exit setIsEnabled:YES];
	
	numLabel.visible = YES;
	
	
	[self schedule:@selector(step:)];
	

	[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"GameWin1.aif" loop:YES];
	
}

#pragma mark -
- (void)exitFunc: (id)sender
{
	[[SimpleAudioEngine sharedEngine] playEffect:@"Cancel.aif"];
	
	
	switch (m_state)
	{
		case 1:
		{
			m_lock = [[NSUserDefaults standardUserDefaults] integerForKey:@"statelockKey2"];
			m_lock = NO;
			[[NSUserDefaults standardUserDefaults] setBool:m_lock forKey:@"statelockKey2"];
		}
			
			break;
		case 2:
		{
			m_lock = [[NSUserDefaults standardUserDefaults] integerForKey:@"statelockKey3"];
			m_lock = NO;
			[[NSUserDefaults standardUserDefaults] setBool:m_lock forKey:@"statelockKey3"];
		}
			
			break;
		case 3:
		{
			m_lock = [[NSUserDefaults standardUserDefaults] integerForKey:@"statelockKey4"];
			m_lock = NO;
			[[NSUserDefaults standardUserDefaults] setBool:m_lock forKey:@"statelockKey4"];
		}
			
			break;
		case 4:
		{
			m_lock = [[NSUserDefaults standardUserDefaults] integerForKey:@"statelockKey5"];
			m_lock = NO;
			[[NSUserDefaults standardUserDefaults] setBool:m_lock forKey:@"statelockKey5"];
		}
			
			break;
		case 5:
		{
			m_lock = [[NSUserDefaults standardUserDefaults] integerForKey:@"statelockKey6"];
			m_lock = NO;
			[[NSUserDefaults standardUserDefaults] setBool:m_lock forKey:@"statelockKey6"];
		}
			
			break;
		case 6:
		{
			m_lock = [[NSUserDefaults standardUserDefaults] integerForKey:@"statelockKey7"];
			m_lock = NO;
			[[NSUserDefaults standardUserDefaults] setBool:m_lock forKey:@"statelockKey7"];
		}
			
			break;
		case 7:
		{
			m_lock = [[NSUserDefaults standardUserDefaults] integerForKey:@"statelockKey8"];
			m_lock = NO;
			[[NSUserDefaults standardUserDefaults] setBool:m_lock forKey:@"statelockKey8"];
		}
			
			break;
		case 8:
		{
			m_lock = [[NSUserDefaults standardUserDefaults] integerForKey:@"statelockKey9"];
			m_lock = NO;
			[[NSUserDefaults standardUserDefaults] setBool:m_lock forKey:@"statelockKey9"];
		}
			
			break;
		case 9:
		{
			
		}
			
			break;
		default:
			break;
	}
	
	m_index = m_state-1;
	[[NSUserDefaults standardUserDefaults] setInteger:m_index forKey: @"GameStage"];
	
	[self unscheduleAllSelectors];
	[self removeAllChildrenWithCleanup:YES];
	[[CCDirector sharedDirector] replaceScene: [MainScene scene]];
}
#pragma mark -
- (void)nextFunc: (id)sender
{
	[[SimpleAudioEngine sharedEngine] playEffect:@"Confirm.aif"];
	[[SimpleAudioEngine sharedEngine] playEffect:@"Replacescene.aif"];
	switch (m_state)
	{
		case 1:
		{
			m_lock = [[NSUserDefaults standardUserDefaults] integerForKey:@"statelockKey2"];
			m_lock = NO;
			[[NSUserDefaults standardUserDefaults] setBool:m_lock forKey:@"statelockKey2"];
		}
			
			break;
		case 2:
		{
			m_lock = [[NSUserDefaults standardUserDefaults] integerForKey:@"statelockKey3"];
			m_lock = NO;
			[[NSUserDefaults standardUserDefaults] setBool:m_lock forKey:@"statelockKey3"];
		}
			
			break;
		case 3:
		{
			m_lock = [[NSUserDefaults standardUserDefaults] integerForKey:@"statelockKey4"];
			m_lock = NO;
			[[NSUserDefaults standardUserDefaults] setBool:m_lock forKey:@"statelockKey4"];
		}
			
			break;
		case 4:
		{
			m_lock = [[NSUserDefaults standardUserDefaults] integerForKey:@"statelockKey5"];
			m_lock = NO;
			[[NSUserDefaults standardUserDefaults] setBool:m_lock forKey:@"statelockKey5"];
		}
			
			break;
		case 5:
		{
			m_lock = [[NSUserDefaults standardUserDefaults] integerForKey:@"statelockKey6"];
			m_lock = NO;
			[[NSUserDefaults standardUserDefaults] setBool:m_lock forKey:@"statelockKey6"];
		}
			
			break;
		case 6:
		{
			m_lock = [[NSUserDefaults standardUserDefaults] integerForKey:@"statelockKey7"];
			m_lock = NO;
			[[NSUserDefaults standardUserDefaults] setBool:m_lock forKey:@"statelockKey7"];
		}
			
			break;
		case 7:
		{
			m_lock = [[NSUserDefaults standardUserDefaults] integerForKey:@"statelockKey8"];
			m_lock = NO;
			[[NSUserDefaults standardUserDefaults] setBool:m_lock forKey:@"statelockKey8"];
		}
			
			break;
		case 8:
		{
			m_lock = [[NSUserDefaults standardUserDefaults] integerForKey:@"statelockKey9"];
			m_lock = NO;
			[[NSUserDefaults standardUserDefaults] setBool:m_lock forKey:@"statelockKey9"];
		}
			
			break;
		case 9:
		{

		}
			
			break;
		default:
			break;
	}
	m_index = m_state-1;
	[[NSUserDefaults standardUserDefaults] setInteger:m_index forKey: @"GameStage"];
	
	m_state++;
	[[NSUserDefaults standardUserDefaults] setInteger:m_state forKey: @"GameState"];
	
	
	[self unscheduleAllSelectors];
	[self removeAllChildrenWithCleanup:YES];
	if (m_state !=7)
	{
		[[CCDirector sharedDirector] replaceScene: [GameStory scene]];
	}
	else
	{
		[[CCDirector sharedDirector] replaceScene: [GameScene scene]];
	}

	
}

#pragma mark -
- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *touch = [touches anyObject];
	CGPoint touchLocation = [touch locationInView:[touch view]];
	touchLocation = [[CCDirector sharedDirector] convertToGL:touchLocation];
	getnum = m_num;

	
}
#pragma mark -
- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *touch = [touches anyObject];
	CGPoint touchLocation = [touch locationInView:[touch view]];
	touchLocation = [[CCDirector sharedDirector] convertToGL:touchLocation];
	[self unschedule:@selector(step:)];
	
	if (falldone == NO)
	{
		switch (m_state)
		{
				
			case 3:
			{
				[self touchtogo];
			}
				break;
				
			case 6:
			{
				
				[self touchtogo];
			}
				break;
				
				
			case 9:
			{
				[self touchtogo];		
			}
				break;
			default:
				break;
		}
		
		falldone = YES;
	}
	

	
}




#pragma mark -
- (void)dealloc
{	
	[super dealloc];	
}

@end
