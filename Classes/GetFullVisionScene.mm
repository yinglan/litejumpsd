//
//  GetFullVisionScene.m
//  PopoJump
//
//  Created by o0402 on 11-8-22.
//  Copyright 2011 inblue. All rights reserved.
//

#import "GetFullVisionScene.h"
#import "MainScene.h"

@implementation GetFullVisionScene

+(id) scene
{ 
	CCScene *scene = [CCScene node]; 
	GetFullVisionScene *layer = [GetFullVisionScene node]; 
	[scene addChild: layer]; 
	return scene;
}

-(id) init
{ 
	if( (self=[super init] )) 
	{ 
		CCSprite *back = [CCSprite spriteWithFile:@"lite_open_stage.png"];
		back.position = ccp(160,240);
		[self addChild:back];
		
		CCMenuItem *getfull = [CCMenuItemImage itemFromNormalImage:@"unlimited_button.png"
									  selectedImage:@"unlimited_button_down.png"
											 target:self
										   selector:@selector(getFullVision:)];
		getfull.position = ccp(104,39);
		CCMenuItem *backButton = [CCMenuItemImage itemFromNormalImage:@"back_button.png"
													 selectedImage:@"back_button_down.png"
															target:self
														  selector:@selector(backMainScene:)];
		backButton.position = ccp(254,39);
		CCMenu *menu = [CCMenu menuWithItems:getfull,backButton,nil];
		menu.position = ccp(0,0);
		[self addChild:menu];
		
		
		visionShow = [CCSprite spriteWithFile:@"lite_open_stage_vision.png"];
		visionShow.position = ccp(160,240);
		[self addChild:visionShow z:5];
		visionShow.visible = NO;
		
		CCMenuItem *getHD = [CCMenuItemImage itemFromNormalImage:@"getit.png"
												   selectedImage:@"getit_down.png"
														  target:self
														selector:@selector(getHDVision:)];
		getHD.position = ccp(91,182);
		CCMenuItem *getSD = [CCMenuItemImage itemFromNormalImage:@"getit.png"
												   selectedImage:@"getit_down.png"
														  target:self
														selector:@selector(getSDVision:)];
		getSD.position = ccp(231,182);
		menu1 = [CCMenu menuWithItems:getHD,getSD,nil];
		menu1.position = ccp(0,0);
		[self addChild:menu1 z:6];
		menu1.visible = NO;
		
		leftButton = [CCMenuItemImage itemFromNormalImage:@"lb_lite_L.png"
									   selectedImage:@"lb_lite_L_down.png"
											  target:self
											selector:@selector(skipL:)];
		[leftButton setPosition:ccp(30,270)];
 		rightButton = [CCMenuItemImage itemFromNormalImage:@"lb_lite_R.png"
									   selectedImage:@"lb_lite_R_down.png"
											  target:self
											selector:@selector(skipR:)];
		[rightButton setPosition:ccp(290,270)];
 		CCMenu *menuLRButton = [CCMenu menuWithItems:leftButton,rightButton,nil];	
		menuLRButton.position = ccp(0,0); 
		[self addChild:menuLRButton z:3];
		
		for (int i = 0; i < 6; i++)
		{
			NSString *file = [NSString stringWithFormat:@"lite_show_%d.png",i+1];
			show[i] = [CCSprite spriteWithFile:file];
			show[i].position = ccp(160,265);
			show[i].visible = NO;
			[self addChild:show[i] z:3];
		}
		show[0].visible = YES;
		m_nCurrentShowIndex = 0;
	}
	return self;
}

- (void)skipL:(id)sender
{
	for (int i = 0; i < 6; i++)
	{
		show[i].visible = NO;
	}
	m_nCurrentShowIndex--;
	if (m_nCurrentShowIndex < 0)
		m_nCurrentShowIndex = 0;
	show[m_nCurrentShowIndex].visible = YES;
}

- (void)skipR:(id)sender
{
	for (int i = 0; i < 6; i++)
	{
		show[i].visible = NO;
	}
	m_nCurrentShowIndex++;
	if (m_nCurrentShowIndex > 5)
		m_nCurrentShowIndex = 5;
	show[m_nCurrentShowIndex].visible = YES;
}

-(void) getFullVision:(id)sender
{
	visionShow.visible = !visionShow.visible;
	menu1.visible = !menu1.visible;
}

-(void) getHDVision:(id)sender
{ 
	NSURL *url = [NSURL URLWithString:@"itms://itunes.apple.com/cn/app/id447313840?mt=8"];
	[[UIApplication sharedApplication] openURL:url];
}

-(void) getSDVision:(id)sender
{ 
	NSURL *url = [NSURL URLWithString:@"itms://itunes.apple.com/cn/app/id438380586?mt=8"];
	[[UIApplication sharedApplication] openURL:url]; 
}

-(void) backMainScene:(id)sender
{
	//[[SimpleAudioEngine sharedEngine] playEffect:@"Confirm.aif"];
	//[[SimpleAudioEngine sharedEngine] playEffect:@"Replacescene.aif"];
	
	[self unscheduleAllSelectors];
	[self removeAllChildrenWithCleanup:YES];
	//[[CCDirector sharedDirector] purgeCachedData];
	//[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[MainScene scene] withColor:ccWHITE]]; 
	[[CCDirector sharedDirector] replaceScene:[MainScene scene]]; 
}

- (void)dealloc
{
	[super dealloc];
}

@end
