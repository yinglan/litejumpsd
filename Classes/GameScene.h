//
//  GameScene.h
//  FatJumper
//
//  Created by in-blue  on 11-2-11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameOver.h"
#import "GameWin.h"  
#import "Main.h"
#import "GameWin.h"
#import "PauseScene.h"
#define MIN_FALL_INDEX 65535

#define SIXTY_COMBO		60
#define TEN_COMBO		10
#define TWENTY_COMBO	20
#define FORTY_COMBO		40
#define EIGHTY_COMBO	80

@interface GameScene : Main 
{

	CGPoint man_pos;
	CGPoint man_vel;
	CGPoint man_acc;	

	CCLabelAtlas *hightLabel;//高度记录
	CCLabelAtlas *numLabel;//金币计数
	
	CCSprite *p_icon_ballon;
	CCSprite *p_icon_shoe;
	CCSprite *p_icon_wing;
	CCSprite *p_icon_effect;
	CCSprite *p_icon_count;
	CCSprite *p_tap;
	
	CCMenuItem *item_pause; 

	
	CCMenu *tapmenu;
	CCMenuItem *item_tap;
	

	
	CCSprite *p_loading_bg;
	CCSprite *p_loading_mouth;
	CCSprite *p_loading_teeth;
	CCSprite *p_loading_eyes;
	int m_x1;
	int m_x2;
	
	int easyjudge;
	int mediumjudge;
	int hardjudge;
	int continuousjudge1;
	int continuousjudge2;
	int continuousjudge3;
	
	int rainsjudge;
	int blinkjudge;
	int rocketjudge;
	int blocksjudge;
	int funsjudge;
	int mixsjudge;
	int ghostsjudge;
    int judge;
	
	
	CCSprite *p_man;
	
	int m_player;
	int m_hightscore;//分数
	int m_moneyscore;
	int m_hamburgcount;
	int m_kind;
	
	int mixcount;
	int continuouscount;
	int musiccount;
	int rainscount1;
	int rainscount2;
	int rainscount3;
	int blinkcount1;
	int blinkcount2;
	int blinkcount3;
	int rocketcount1;
	int rocketcount2;
	int blockscount1;
	int blockscount2;
	int blockscount3;
	int funscount;
	int mixscount;
	int ghostscount;
	
	float m_lastTime;//下落持续时间
	float m_flowTime;//漂浮时间
	float m_flyTime;//飞行时间
	float m_wingTime;
	float m_shoesTime;
	float m_dashTime;
	float m_ghostTime;
	float m_shakeTime;
	float m_rainTime;
	float m_frozenTime;
	float m_moneyTime;
	int totalcount;
	CCSprite *p_ready;
	CCSprite *p_go;

	
	CCSprite *tagdash;
	CCSprite *tagwing;
	CCSprite *tagballon;
	CCSprite *tagshoe;
	CCSprite *tagIcon;
	
	CCSprite *p_tf;
	CCSprite *p_tftf;
	CCSprite *p_tf2;
	CCSprite *p_tf3;
	CCSprite *p_tf4;
	CCSprite *p_tf5;
	CCSprite *p_tfmoney;
	CCSprite *p_tfrain;
	CCSprite *p_tffrozen;
	
	CCSprite *particleFly;
	CCSprite *particleFall;
	long	 m_lFallParCount;
	CCSprite *particleDash;
	CCSprite *particleBomb;
	CCSprite *particleGhost;

	
	BOOL gameSuspended;//游戏是否中断
	BOOL manLookingRight;//默认方向 右
	BOOL gameWillStart;//游戏将开始
	BOOL manWillFall;//是否将要下落
	BOOL manWalking;//是否在行走
	BOOL manWillDead;//是否将要死亡
	BOOL manDead;//是否死亡
	BOOL manFlow;//人物是否处于漂浮状态
	BOOL manFly;//人物是否处于冲刺状态
	BOOL manrush;
	BOOL manWing;
	BOOL manShoes;
	BOOL manDash;
	BOOL manBomb;
	BOOL manGhost;
	BOOL manshake;
	BOOL manRain;
	BOOL manFrozen;
	BOOL blocks1;
	BOOL blocks2;
	BOOL blocks3;
	BOOL blinks2;
	BOOL blinks3;
	BOOL continuous1;
	BOOL continuous2;
	BOOL continuous3;
	BOOL rockets1;
	BOOL rockets2;
	BOOL createDone;
	BOOL rains1;
	BOOL rains2;
	BOOL rains3;
	BOOL moneys;
	BOOL funs;
	BOOL mixs;
	BOOL ghosts;
	CCSprite *p_power_dash;
	CCSprite *p_power_shoe;
	CCSprite *p_power_wing;
	CCSprite *p_power_ballon;
	CCSprite *p_power_fly;

	CCParticleSystem	*emitter;
	BOOL showtag;
	BOOL mapmove;
	BOOL cross;
	
	// 小新
	unsigned int			m_nComboCount;			// 连击统计
	bool					m_bGameStart; 
	unsigned int			m_nTimeCountLeft;           // 作弊时间统计
	unsigned int			m_nTimeCountRight;           // 作弊时间统计
	
	
	int shoecount;
	int wingcount;
	int dashcount;
	int balloncount;
	int frozencount;
	int bombcount;
	int boltcount;
	int ghostcount;
	int blockcount;
	int hatcount;
}
- (id)initWithdata:(BOOL)endless:(int)player:(int)state;
- (void)initItem;
+ (id)scene;
+(GameScene *) sendTopause;
- (void)loading;
- (void)gameReady;
- (void)gameStart:(id)sender;
- (void)logic: (ccTime)t;
- (void)update: (ccTime)t;
- (void)continueGame:(id)sender;
- (void)pauseGame;
- (void)initpauseUI;
- (void)initMan;
- (void)resetMan;
- (void)continueGame;
- (void)startGame;
- (void)initProp;
- (void)blockEffect;
- (void)frozenEffect;
- (void)shakeEffect;
- (void)ghostEffect;
- (void)bombEffect;

- (void)jumpEffect;
- (void)jumpagainEffect;
- (void)fallEffect;
- (void)deadEffect;
- (void)flyEffect;
- (void)flowEffect;
- (void)wingEffect;
- (void)rainEffect;
- (void)shoesEffect;
- (void)dashEffect;

- (void)moneyEffect;
- (void)tagEffect;
- (void)rushEffect;
- (void)showGameover;
- (void)judgenext:(int)count;


- (void)reloadEasy1Prop:(int)count;
- (void)reloadEasy2Prop:(int)count;
- (void)reloadEasy3Prop:(int)count;


- (void)reloadMedium1Prop:(int)count;
- (void)reloadMedium2Prop:(int)count;
- (void)reloadMedium3Prop:(int)count;


- (void)reloadHard1Prop:(int)count;
- (void)reloadHard2Prop:(int)count;
- (void)reloadHard3Prop:(int)count;

- (void)reloadeasy6:(int)count;
- (void)reloadmedium6:(int)count;
- (void)reloadhard6:(int)count;

- (void)reloadEasy11Prop:(int)count;
- (void)reloadMedium11Prop:(int)count;
- (void)initSound;
- (void)unlockAC; 

@end
