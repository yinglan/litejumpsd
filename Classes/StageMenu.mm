//
//  ChoiceMenu.m
//  tweejump
//
//  Created by inblue-piepie on 10-7-23.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "StageMenu.h"


@implementation StageMenu


+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	StageMenu *layer = [StageMenu node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	
	
	// return the scene
	return scene;
}

-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init] )) 
	{
		
		BOOL endless = [[NSUserDefaults standardUserDefaults] integerForKey:@"GameEndless"];
		
		[self initWithdata:endless];	
		//[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"ui_music.mp3" loop:YES]; 
	}
	return self;
}



- (id)initWithdata:(BOOL)endless
{

	m_endless = endless;
	p_bg = [CCSprite spriteWithFile:@"choose_bg_board.png"];
	[self addChild:p_bg z:1];
	[p_bg setPosition:ccp(161,239)];

	p_bg_front = [CCSprite spriteWithFile:@"choose_front_board_black.png"];
	[p_bg addChild:p_bg_front z:-4];
	[p_bg_front setPosition:ccp(157,330)];
	
	map_state[0].p_map_state= [CCSprite spriteWithFile:@"state_05_icon.png"];
	[p_bg addChild:map_state[0].p_map_state z:-4];
	[map_state[0].p_map_state setPosition:ccp(160,109)];
	
	map_state[2].p_map_state= [CCSprite spriteWithFile:@"state_19_icon.png"];
	[p_bg addChild:map_state[2].p_map_state z:-4];
	[map_state[2].p_map_state setPosition:ccp(160,109)];
	
	map_state[1].p_map_state= [CCSprite spriteWithFile:@"state_14_icon.png"];
	[p_bg addChild:map_state[1].p_map_state z:-4];
	[map_state[1].p_map_state setPosition:ccp(160,109)];//309
	
	
	p_map_lock = [CCSprite spriteWithFile:@"stage_lock.png"];
	[p_bg addChild:p_map_lock z:-2];
	[p_map_lock setPosition:ccp(158,330)];
	id action1 = [CCFadeOut actionWithDuration:1.0];
	id action2 = [CCFadeIn actionWithDuration:1.0];
	[p_map_lock runAction:[CCRepeatForever actionWithAction:[CCSequence actions:action1,action2,nil]]];
	
//	p_mapLock[0] = [CCSprite spriteWithFile:@"stage_lock1.png"];
//	[p_bg addChild:p_mapLock[0] z:-3];
//	[p_mapLock[0] setPosition:ccp(161,325)];
//	p_mapLock[0].visible = NO;
//	
//	p_mapLock[1] = [CCSprite spriteWithFile:@"stage_lock2.png"];
//	[p_bg addChild:p_mapLock[1] z:-3];
//	[p_mapLock[1] setPosition:ccp(161,325)];
//	p_mapLock[1].visible = NO;
//	
//	p_mapLock[2] = [CCSprite spriteWithFile:@"stage_lock3.png"];
//	[p_bg addChild:p_mapLock[2] z:-3];
//	[p_mapLock[2] setPosition:ccp(161,325)];
//	p_mapLock[2].visible = NO;
	
	firstTime = [[NSUserDefaults standardUserDefaults] boolForKey:@"onetimesstage"];
	if (firstTime == NO)
	{
		m_index = 0;
		map_state[0].islock = NO;
		map_state[1].islock = YES;
		map_state[2].islock = YES;
		map_state[3].islock = YES;
		map_state[4].islock = YES;
		map_state[5].islock = YES;
		map_state[6].islock = YES;
		map_state[7].islock = YES;
		map_state[8].islock = YES;

		firstTime = YES;
		[[NSUserDefaults standardUserDefaults] setBool:firstTime forKey:@"onetimesstage"];
		
		[[NSUserDefaults standardUserDefaults] setBool:map_state[0].islock forKey:@"statelockKey1"];
		[[NSUserDefaults standardUserDefaults] setBool:map_state[1].islock forKey:@"statelockKey2"];
		[[NSUserDefaults standardUserDefaults] setBool:map_state[2].islock forKey:@"statelockKey3"];
		[[NSUserDefaults standardUserDefaults] setBool:map_state[3].islock forKey:@"statelockKey4"];
		[[NSUserDefaults standardUserDefaults] setBool:map_state[4].islock forKey:@"statelockKey5"];
		[[NSUserDefaults standardUserDefaults] setBool:map_state[5].islock forKey:@"statelockKey6"];
		[[NSUserDefaults standardUserDefaults] setBool:map_state[6].islock forKey:@"statelockKey7"];
		[[NSUserDefaults standardUserDefaults] setBool:map_state[7].islock forKey:@"statelockKey8"];
		[[NSUserDefaults standardUserDefaults] setBool:map_state[8].islock forKey:@"statelockKey9"];
		
		

	}
	
	else
	{
		m_index = [[NSUserDefaults standardUserDefaults] integerForKey:@"GameStage"];
		
		map_state[0].islock = [[NSUserDefaults standardUserDefaults] boolForKey:@"statelockKey1"];
		map_state[1].islock = [[NSUserDefaults standardUserDefaults] boolForKey:@"statelockKey2"];
		map_state[2].islock = [[NSUserDefaults standardUserDefaults] boolForKey:@"statelockKey3"];
		map_state[3].islock = [[NSUserDefaults standardUserDefaults] boolForKey:@"statelockKey4"];
		map_state[4].islock = [[NSUserDefaults standardUserDefaults] boolForKey:@"statelockKey5"];
		map_state[5].islock = [[NSUserDefaults standardUserDefaults] boolForKey:@"statelockKey6"];
		map_state[6].islock = [[NSUserDefaults standardUserDefaults] boolForKey:@"statelockKey7"];
		map_state[7].islock = [[NSUserDefaults standardUserDefaults] boolForKey:@"statelockKey8"];
		map_state[8].islock = [[NSUserDefaults standardUserDefaults] boolForKey:@"statelockKey9"];

		
	}

	toRight = YES;
	[self initSound];
	[self initMap];
	[self initItem];
	[self judge:m_index];
	[self showMap];
	
	return self;
}
#pragma mark -
- (void)initItem
{
	p_arrowL = [CCMenuItemImage itemFromNormalImage:@"Arrow_button_L.png"
									  selectedImage:@"Arrow_button_L_down.png"
											 target:self
										   selector:@selector(moveLeft:)];
	
	p_arrowR = [CCMenuItemImage itemFromNormalImage:@"Arrow_button_R.png"
									  selectedImage:@"Arrow_button_R_down.png"
											 target:self
										   selector:@selector(moveRight:)];

	
	p_play = [CCMenuItemImage itemFromNormalImage:@"play_button.png"
									  selectedImage:@"play_button_down.png"
											 target:self
										 selector:@selector(getStage:)];
	
	
	p_unplay = [CCMenuItemImage itemFromNormalImage:@"unplay_button.png"
									selectedImage:@"unplay_button_down.png"
										   target:self
										   selector:@selector(Lock:)];
	
	
	p_backto = [CCMenuItemImage itemFromNormalImage:@"back_button.png"
									  selectedImage:@"back_button_down.png"
											 target:self
										   selector:@selector(back:)];
	
	[p_arrowL setPosition:ccp(-132,101)];
	[p_arrowR setPosition:ccp(126,101)];
	[p_play setPosition:ccp(30,-100)];
	[p_unplay setPosition:ccp(30,-100)];
	[p_backto setPosition:ccp(30,-160)];
	
	CCMenu *menu = [CCMenu menuWithItems:p_unplay,p_play,p_arrowL,p_arrowR,p_backto,nil];
	[p_bg addChild:menu z:2];
	
	p_play_icon = [CCSprite spriteWithFile:@"play_icon.png"];
	[p_bg addChild:p_play_icon z:2];
	[p_play_icon setPosition:ccp(103,140)];
	

	
	p_bg_front = [CCSprite spriteWithFile:@"black_board.png"];
	[p_bg addChild:p_bg_front z:-1];
	[p_bg_front setPosition:ccp(157.5,225.5)];
	
	
	
	
	p_state[0] = [CCSprite spriteWithFile:@"map_stage1.png"];
	[p_bg addChild:p_state[0] z:-1];
	[p_state[0] setPosition:ccp(160,160)];
	
	p_state[1] = [CCSprite spriteWithFile:@"map_stage2.png"];
	[p_bg addChild:p_state[1] z:-1];
	[p_state[1] setPosition:ccp(160,160)];
	
	p_state[2] = [CCSprite spriteWithFile:@"map_stage3.png"];
	[p_bg addChild:p_state[2] z:-1];
	[p_state[2] setPosition:ccp(160,160)];
	
	p_state[3] = [CCSprite spriteWithFile:@"map_stage4.png"];
	[p_bg addChild:p_state[3] z:-1];
	[p_state[3] setPosition:ccp(160,160)];
	
	p_state[4] = [CCSprite spriteWithFile:@"map_stage5.png"];
	[p_bg addChild:p_state[4] z:-1];
	[p_state[4] setPosition:ccp(160,160)];
	
	p_state[5] = [CCSprite spriteWithFile:@"map_stage6.png"];
	[p_bg addChild:p_state[5] z:-1];
	[p_state[5] setPosition:ccp(160,160)];
	

	p_state[6] = [CCSprite spriteWithFile:@"map_stage7.png"];
	[p_bg addChild:p_state[6] z:-1];
	[p_state[6] setPosition:ccp(160,160)];
	
	p_state[7] = [CCSprite spriteWithFile:@"map_stage8.png"];
	[p_bg addChild:p_state[7] z:-1];
	[p_state[7] setPosition:ccp(160,160)];
	
	p_state[8] = [CCSprite spriteWithFile:@"map_stage9.png"];
	[p_bg addChild:p_state[8] z:-1];
	[p_state[8] setPosition:ccp(160,160)];
	
	CCSprite *p_yellow =  [CCSprite spriteWithFile:@"choose_bg_yellow.png"];
	[self addChild:p_yellow z:0];
	[p_yellow setPosition:ccp(160,103)];
	
	p_small_lock = [CCSprite spriteWithFile:@"small_lock.png"];
	[self addChild:p_small_lock z:1];
	[p_small_lock setPosition:ccp(245,430)];
	p_small_lock.visible = NO;
	
	p_small_open = [CCSprite spriteWithFile:@"small_open.png"];
	[self addChild:p_small_open z:1];
	[p_small_open setPosition:ccp(245,430)];
	p_small_open.visible = NO;
}



#pragma mark -
- (void)showMap
{
	switch (m_index)
	{
		case 0:
		{		
			[p_state[0] setPosition:ccp(160,222)];
			[map_state[0].p_map_state setPosition:ccp(158,330)];
			
		}
			break;
		case 1:
		{		
			[p_state[1] setPosition:ccp(160,222)];
			[map_state[0].p_map_state setPosition:ccp(158,330)];
			
		}
			break;
		case 2:	
		{		
			[p_state[2] setPosition:ccp(160,222)];
			[map_state[0].p_map_state setPosition:ccp(158,330)];
			
		}
			break;
		case 3:
		{		
			[p_state[3] setPosition:ccp(160,222)];
			[map_state[1].p_map_state setPosition:ccp(158,330)];
			
		}
			break;
		case 4:
		{		
			[p_state[4] setPosition:ccp(160,222)];
			[map_state[1].p_map_state setPosition:ccp(158,330)];
			
		}
			break;
		case 5:	
		{
			[p_state[5] setPosition:ccp(160,222)];
			[map_state[1].p_map_state setPosition:ccp(158,330)];			
		}
			break;
			
		case 6:
		{
			[p_state[6] setPosition:ccp(160,222)];
			[map_state[2].p_map_state setPosition:ccp(158,330)];				
		}
			break;	
		case 7:
		{
			[p_state[7] setPosition:ccp(160,222)];
			[map_state[2].p_map_state setPosition:ccp(158,330)];				
		}
			break;	
		case 8:	
		{
			[p_state[8] setPosition:ccp(160,222)];
			[map_state[2].p_map_state setPosition:ccp(158,330)];				
		}
			break;	
			
		default:
			break;
	}
	[[SimpleAudioEngine sharedEngine] playEffect:@"Changestage.aif"];
}


#pragma mark -
- (void)moveLeft:(id)sender
{
	
	[[SimpleAudioEngine sharedEngine] playEffect:@"ChoosestageL.aif"];
	
	if(m_index > 0)
	{
		[p_state[m_index] setPosition:ccp(165,180)];
		m_index--;
		
		if (m_index ==2)
		{
			[map_state[1].p_map_state setPosition:ccp(165,109)];			
			
			[map_state[0].p_map_state setPosition:ccp(165,309)];
		}
		if (m_index ==5)
		{
			[map_state[2].p_map_state setPosition:ccp(165,109)];			
		
			[map_state[1].p_map_state setPosition:ccp(165,309)];
		}

		
		toRight = NO;
		[self judge:m_index];
		[self showMap];
		//
		
	}
	
	
	
	
}

#pragma mark -
-(void)moveRight:(id)sender
{
	
	
	[[SimpleAudioEngine sharedEngine] playEffect:@"ChoosestageR.aif"];
	if(m_index < 2)
	{
		[p_state[m_index] setPosition:ccp(165,180)];
		m_index++;
		
		if (m_index ==3)
		{
			[map_state[0].p_map_state setPosition:ccp(165,109)];			
			
		}
		if (m_index ==6)
		{
			[map_state[1].p_map_state setPosition:ccp(165,109)];			
			
		}
		
		toRight = YES;
		
		[self judge:m_index];
		
		[self showMap];

	}

}
#pragma mark -
- (void)judge:(int)m_tag
{
	if (map_state[m_tag].islock == YES)
	{
		p_play.visible = NO;
		[p_play setIsEnabled:NO];
		p_unplay.visible = YES;
		[p_unplay setIsEnabled:YES];
		p_map_lock.visible = YES;
		p_small_lock.visible = YES;
		p_small_open.visible= NO;
//		switch (m_tag)
//		{
//			case 0:
//			case 1:
//			case 2:	
//				p_mapLock[0].visible = YES;
//				p_mapLock[1].visible = NO;
//				p_mapLock[2].visible = NO;
//				break;
//			case 3:
//			case 4:
//			case 5:	
//				p_mapLock[0].visible = NO;
//				p_mapLock[1].visible = YES;
//				p_mapLock[2].visible = NO;
//				break;
//			case 6:
//			case 7:
//			case 8:	
//				p_mapLock[0].visible = NO;
//				p_mapLock[1].visible = NO;
//				p_mapLock[2].visible = YES;
//				break;
//			default:
//				break;
//		}
	}
	else
	{
		p_play.visible = YES;
		[p_play setIsEnabled:YES];
		p_unplay.visible = NO;
		[p_unplay setIsEnabled:NO];
		p_map_lock.visible = NO;
		p_small_lock.visible = NO;
		p_small_open.visible = YES;
//		p_mapLock[0].visible = NO;
//		p_mapLock[1].visible = NO;
//		p_mapLock[2].visible = NO;
	
	}
	
	if (toRight)
	{
		switch (m_tag)
		{
			case 0:
			{
				
				CCAnimation *p_change = [CCAnimation animationWithName:@"change" delay:0.05];
				for (int i=2; i<6; i++) 
				{
					[p_change addFrameWithFilename:[NSString stringWithFormat:@"state_%02d_icon.png", i]];
				}
				[map_state[0].p_map_state runAction:[CCAnimate actionWithAnimation:p_change restoreOriginalFrame:NO]];
				[[SimpleAudioEngine sharedEngine] playEffect:@"Changestagerect.aif"];
				
			}
				break;
			case 3:
			{
				
				CCAnimation *p_change = [CCAnimation animationWithName:@"change" delay:0.05];
				for (int i=6; i<15; i++) 
				{
					[p_change addFrameWithFilename:[NSString stringWithFormat:@"state_%02d_icon.png", i]];
				}
				[map_state[1].p_map_state runAction:[CCAnimate actionWithAnimation:p_change restoreOriginalFrame:NO]];
				[[SimpleAudioEngine sharedEngine] playEffect:@"Changestagerect.aif"];
				
			}
				break;
			case 6:
			{
				
				CCAnimation *p_change = [CCAnimation animationWithName:@"change" delay:0.05];
				for (int i=15; i<20; i++) 
				{
					[p_change addFrameWithFilename:[NSString stringWithFormat:@"state_%02d_icon.png", i]];
				}
				[map_state[2].p_map_state runAction:[CCAnimate actionWithAnimation:p_change restoreOriginalFrame:NO]];
				[[SimpleAudioEngine sharedEngine] playEffect:@"Changestagerect.aif"];
				
			}
				break;
			default:
				break;
		}
	}
	
	
	else
	{
		switch (m_tag)
		{
			case 2:
			{
				
				CCAnimation *p_change = [CCAnimation animationWithName:@"change" delay:0.05];
				for (int i=13; i>4; i--) 
				{
					[p_change addFrameWithFilename:[NSString stringWithFormat:@"state_%02d_icon.png", i]];
				}
				[map_state[0].p_map_state runAction:[CCAnimate actionWithAnimation:p_change restoreOriginalFrame:NO]];
				[[SimpleAudioEngine sharedEngine] playEffect:@"Changestagerect.aif"];
				
			}
				break;
			case 5:
			{
				
				CCAnimation *p_change = [CCAnimation animationWithName:@"change" delay:0.05];
				for (int i=19; i>13; i--) 
				{
					[p_change addFrameWithFilename:[NSString stringWithFormat:@"state_%02d_icon.png", i]];
				}
				[map_state[1].p_map_state runAction:[CCAnimate actionWithAnimation:p_change restoreOriginalFrame:NO]];
				[[SimpleAudioEngine sharedEngine] playEffect:@"Changestagerect.aif"];
				
			}
				break;
	
			default:
				break;
		}
	}



}
#pragma mark -
- (void)Lock:(id)sender
{
	[[SimpleAudioEngine sharedEngine] playEffect:@"Lock.aif"];
}

#pragma mark -
- (void)getStage:(id)sender
{
	
	[[SimpleAudioEngine sharedEngine] playEffect:@"GetStage.aif"];
		
	switch (m_index)
	{
		case 0:
		
			m_state = kStage_1;
			
			break;
		case 1:
		
			m_state = kStage_2;
		
			break;
			
		case 2:
			
			m_state = kStage_3;
			
			break;
			
		case 3:
			
			m_state = kStage_4;
			
			break;
		case 4:
			
			m_state = kStage_5;
			
			break;
			
		case 5:
			
			m_state = kStage_6;
			
			break;
		
		case 6:
			
			m_state = kStage_7;
			
			break;
		case 7:
			
			m_state = kStage_8;
			
			break;
			
		case 8:
			
			m_state = kStage_9;
			
			break;
		default:
			break;
	}
	[[NSUserDefaults standardUserDefaults] setInteger:m_state forKey:@"GameState"];
	[[SimpleAudioEngine sharedEngine] playEffect:@"Replacescene.aif"];
	[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
	[self unscheduleAllSelectors];
	[self removeAllChildrenWithCleanup:YES];
	if (m_index != 6)
	{
		[[CCDirector sharedDirector] replaceScene: [GameStory scene]];
	}
	else
	{
		[[CCDirector sharedDirector] replaceScene: [GameScene scene]];
	}

	
}

#pragma mark -
- (void)back:(id)sender
{
	[[SimpleAudioEngine sharedEngine] playEffect:@"Cancel.aif"];
	[[SimpleAudioEngine sharedEngine] playEffect:@"Replacescene.aif"];
	[[NSUserDefaults standardUserDefaults] setInteger:m_index forKey:@"GameStage"];
	[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
	[self unscheduleAllSelectors];
	[self removeAllChildrenWithCleanup:YES];
	[[CCDirector sharedDirector] replaceScene: [MainScene scene]];
	
}


#pragma mark -
- (void)initMap
{

	p_backmapNode = [CCNode node];
	[self addChild:p_backmapNode z:-1];
	
	p_backNum = [[NSMutableArray alloc] initWithCapacity:10];
	
	for (int i=0;i<6;i++)
	{
		p_backSprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"stage2_scense_bg_%02d.png",i+1]];
		[p_backSprite setPosition:ccp(160,478*i+240)];
		[p_backmapNode addChild:p_backSprite z:-1];
		[p_backNum addObject:p_backSprite];
		[p_backSprite.texture setAliasTexParameters];
	}
	
	id backgoUp = [CCRepeatForever actionWithAction:
				   [CCSequence actions:
					[CCMoveBy actionWithDuration:120 position:ccp(0,-480)],
					[CCCallFunc actionWithTarget:self selector:@selector(backCallback)],
					nil]];
	p_backSpeed = [CCSpeed actionWithAction:backgoUp speed:1];
	[p_backmapNode runAction:p_backSpeed];
	p_backSpeed.speed = 11;

	
}
-(void)initSound
{
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Changestage.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"ChoosestageL.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"ChoosestageR.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Changestagerect.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Lock.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"GetStage.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Replacescene.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Cancel.aif"];
}

#pragma mark -
- (void)backCallback
{
	
	CCSprite *tempMap = [p_backNum objectAtIndex:m_mapCurrentIndex_back%6];
	tempMap.position = ccp(tempMap.position.x,tempMap.position.y + 478*6);
	m_mapCurrentIndex_back++;		

	
}

#pragma mark -
- (void)dealloc 
{
	[[CCTextureCache sharedTextureCache] removeAllTextures];
	[p_backNum release];
	p_backNum = nil;
	[super dealloc];
}



@end
