//
//  GameOver.m
//  FatJumper
//
//  Created by in-blue  on 10-10-29.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "GameOver.h"


@implementation GameOver
static GameOver *singleInstance = nil;
#pragma mark -
+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GameOver *layer = [GameOver node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	
	
	// return the scene
	return scene;
}
#pragma mark -
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init] )) 
	{
		int player = [[NSUserDefaults standardUserDefaults] integerForKey: @"playerKey"];
		BOOL endless = [[NSUserDefaults standardUserDefaults] integerForKey:@"GameEndless"];
		int laststate = [[NSUserDefaults standardUserDefaults] integerForKey:@"GameState"];
		int lastscore = [[NSUserDefaults standardUserDefaults] integerForKey:@"GameScore"];
		int lastnum = [[NSUserDefaults standardUserDefaults] integerForKey:@"Gamemoney"];
		int LastHamburg  = [[NSUserDefaults standardUserDefaults] integerForKey:@"GameHamburg"];
		[self initWithdata:endless :player :laststate :lastscore :lastnum:LastHamburg];
	}
	return self;
}

#pragma mark -
- (id)initWithdata:(BOOL)endless:(int)player:(int)state:(int)Lastscore:(int)Lastnum:(int)LastHamburg
{	
	self.isTouchEnabled = YES; 	
	m_endless = endless;
	m_player = player;
	m_state = state;
	m_score = Lastscore/5;
	m_num = Lastnum;
	m_player = player;
	m_hamburg = LastHamburg;
	singleInstance = self;
	[self initSound];
	m_num += (int)m_hamburg/20;
	getscore = 0;
	getnum = 0;
	
	CCSprite *p_bg = [CCSprite spriteWithFile:@"gameover_bg.png"];
	[self addChild:p_bg z:1];
	p_bg.position = ccp(160,240);
	
	
	CCSprite *p_bg2 = [CCSprite spriteWithFile:@"gameover_bg_board.png"];
	[self addChild:p_bg2 z:2];
	p_bg2.position = ccp(160,720);
	
	retry = [CCMenuItemImage itemFromNormalImage:@"retry_button.png"
								   selectedImage:@"retry_button_down.png"
										  target:self
										selector:@selector(retryFunc:)];
	exit = [CCMenuItemImage itemFromNormalImage:@"quit_button.png"
								  selectedImage:@"quit_button_down.png"
										 target:self
									   selector:@selector(exitFunc:)];
	
	retry.position = ccp(-10,-55);
	[retry setIsEnabled:NO];
	exit.position = ccp(-20,-110);
	[exit setIsEnabled:NO];
	CCMenu *menu = [CCMenu menuWithItems:retry,exit,nil];
	[p_bg2 addChild:menu z:2];
	
	
	open = [CCMenuItemImage itemFromNormalImage:@"openfeint_button.png"
								  selectedImage:@"openfeint_button_down.png"
										 target:self
									   selector:@selector(openfeint:)];
	open.position = ccp(135,-215);
	open.visible = NO;
	CCMenu *menu2 = [CCMenu menuWithItems:open,nil];
	[self addChild:menu2 z:2];
	
	
	
	m_pBtnGameCenter = [CCMenuItemImage itemFromNormalImage:@"gamecenter_button.png"
											  selectedImage:@"gamecenter_button_down.png"
													 target:self
												   selector:@selector(lauchedGameCenter:)];	
	m_pBtnGameCenter.position = ccp(85,-215);
	m_pBtnGameCenter.visible = NO;
	
	m_pGameCenterBack = [CCSprite spriteWithFile:@"gamecenter_back.png"];
	m_pGameCenterBack.position = ccp(145,76);
	m_pGameCenterBack.visible = NO;
	[self addChild:m_pGameCenterBack z:2];
	
	m_pBtnLeaderBoard = [CCMenuItemImage itemFromNormalImage:@"Leaderboard_button.png"
											   selectedImage:@"Leaderboard_button_down.png"
													  target:self
													selector:@selector(lauchedLeaderBoard:)];	
	m_pBtnLeaderBoard.position = ccp(5,-135);
	m_pBtnLeaderBoard.visible = NO;
	
	m_pBtnAchievment = [CCMenuItemImage itemFromNormalImage:@"Achievement_button.png"
											  selectedImage:@"Achievement_button_down.png"
													 target:self
												   selector:@selector(lauchedAchievement:)];	
	m_pBtnAchievment.position = ccp(5,-183);
	m_pBtnAchievment.visible = NO;
	CCMenu* pMenuGC = [CCMenu menuWithItems:m_pBtnGameCenter,m_pBtnLeaderBoard,m_pBtnAchievment,nil];
	[self addChild:pMenuGC z:2];
	
	
	
	
	
	
	hightLabel = [CCLabelAtlas labelWithString:@"000" charMapFile:@"height_number_big.png" itemWidth:22 itemHeight:57 startCharMap:'.'];
	[self addChild:hightLabel z:2 tag:kHightScoreLabel];
	hightLabel.position = ccp(125,250);
	hightLabel.visible = NO;
	
	
	numLabel = [CCLabelAtlas labelWithString:@"000" charMapFile:@"coin_number_big.png" itemWidth:22 itemHeight:57 startCharMap:'.'];
	[self addChild:numLabel z:2 tag:kNumLabel];
	numLabel.position = ccp(165,305);
	numLabel.visible = NO;
	
	[p_bg2 runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.05 position:ccp(160,220)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-22)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,58)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-29)],[CCMoveBy actionWithDuration:0.15 position:ccp(0,21)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-9)],[CCCallFuncN actionWithTarget:self selector:@selector(actionDone:)],nil]];	
	
	[self loadNum];
	[self saveNum];
	[self sumbitHighScore:endless];
	[[SimpleAudioEngine sharedEngine] playEffect:@"GameOverE.aif"];
	

	
	return self;
}
#pragma mark -
- (void)step:(ccTime)dt

{
	if (getnum < m_num)
	{
		getnum++;
	}
	
	if (getscore < m_score)
	{
		getscore++;
	}
	
	NSString *p_hightscoreStr = [NSString stringWithFormat:@"%03d",getnum];
	numLabel = (CCLabelAtlas *)[self getChildByTag:kNumLabel];
	[numLabel setString:p_hightscoreStr];
	
	NSString *numcount = [NSString stringWithFormat:@"%03d",getscore];
	hightLabel = (CCLabelAtlas *)[self getChildByTag:kHightScoreLabel];
	[hightLabel setString:numcount];
	
}

#pragma mark -
- (void)actionDone:(id)sender
{
	[retry setIsEnabled:YES];
	
	[exit setIsEnabled:YES];
	
	hightLabel.visible = YES;
	
	numLabel.visible = YES;
	
	open.visible = YES;
	
	m_pBtnGameCenter.visible = YES;
	
	[self schedule:@selector(step:)];

	[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"GameOver.aif" loop:YES]; 
}
#pragma mark -
- (void)saveNum
{
	m_totalnum += m_num;
	[[NSUserDefaults standardUserDefaults] setInteger:m_totalnum forKey: @"totalNum"];
	m_allcoins += m_num;
}
#pragma mark -
- (void)loadNum
{
	
	m_totalnum = [[NSUserDefaults standardUserDefaults] integerForKey: @"totalNum"];
	
}

#pragma mark -
- (void)sumbitHighScore:(BOOL)endless
{
	if (endless) 
	{
		 		
	} 
}

#pragma mark -
- (void)openfeint: (id)sender
{ 
	//[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
 	
}

+(GameOver *) sendToter
{
	return singleInstance;
}

- (void)lauchedLeaderBoard:(id)sender
{
	//[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
 }

- (void)lauchedAchievement:(id)sender
{
	//[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
 }

- (void)lauchedGameCenter:(id)sender
{
	static BOOL bShowButton = YES;
	m_pBtnLeaderBoard.visible = bShowButton;
	m_pBtnAchievment.visible = bShowButton;
	m_pGameCenterBack.visible = bShowButton;
	//m_pBtnLeaderBoard.scale = 1.5f;
	//m_pBtnAchievment.scale = 1.5f;
	//if (bShowButton) 
	//{
	//	id action1 = [CCScaleTo actionWithDuration:0.2f scale:1.0f];
	//	[m_pBtnAchievment runAction:action1];
	//	id action2 = [CCScaleTo actionWithDuration:0.2f scale:1.0f];
	//	[m_pBtnLeaderBoard runAction:action2];
	//}
	bShowButton = !bShowButton;
}
#pragma mark -
- (void)exitData
{
	if (m_state < 10)
	{
		m_state --;
		[[NSUserDefaults standardUserDefaults] setInteger:m_state forKey: @"GameStage"];
	}
	
	else
	{
		m_state -= 11;
		[[NSUserDefaults standardUserDefaults] setInteger:m_state forKey: @"UnGameStage"];
	}
}
#pragma mark -
- (void)exitFunc: (id)sender
{
	[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
	[[SimpleAudioEngine sharedEngine] playEffect:@"Cancel.aif"];
	[[SimpleAudioEngine sharedEngine] playEffect:@"Replacescene.aif"];
	[self unscheduleAllSelectors];
	[self removeAllChildrenWithCleanup:YES];
	[[CCDirector sharedDirector] replaceScene: [MainScene scene]];
	
	if (m_state < 10)
	{
		[[NSUserDefaults standardUserDefaults] setInteger:m_state forKey: @"GameStage"];
	}
	
	else
	{
		m_state -= 10;
		[[NSUserDefaults standardUserDefaults] setInteger:m_state forKey: @"UnGameStage"];
	}

}
#pragma mark -
- (void)retryFunc: (id)sender
{
	[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
	[[SimpleAudioEngine sharedEngine] playEffect:@"Confirm.aif"];
	[[SimpleAudioEngine sharedEngine] playEffect:@"Replacescene.aif"];
	[self unscheduleAllSelectors];
	[self removeAllChildrenWithCleanup:YES];
	[[CCDirector sharedDirector] replaceScene: [GameScene scene]];
	
}
#pragma mark -

- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *touch = [touches anyObject];
	CGPoint touchLocation = [touch locationInView:[touch view]];
	touchLocation = [[CCDirector sharedDirector] convertToGL:touchLocation];
	getnum = m_num;
	getscore = m_score;
	
}
#pragma mark -
- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *touch = [touches anyObject];
	CGPoint touchLocation = [touch locationInView:[touch view]];
	touchLocation = [[CCDirector sharedDirector] convertToGL:touchLocation];
	[self unschedule:@selector(step:)];
	
}

-(void)initSound
{
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"GameOverE.aif"];
	[[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"GameOver.aif"]; 
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Cancel.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Replacescene.aif"];
}

#pragma mark -

- (void)dealloc
{
	[[CCTextureCache sharedTextureCache] removeAllTextures];
	[super dealloc];
	
}

@end
