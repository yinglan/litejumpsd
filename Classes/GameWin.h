//
//  Gamewin.h
//  FatJumper
//
//  Created by jacko on 11-2-14.
//  Copyright 2011 Inblue.co.ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameScene.h"
 
@interface GameWin : CCLayer
{
	BOOL m_endless;
	int m_state;
	int m_num;
	int m_totalnum;
	int m_allcions;
	int m_player;
	int m_hamburg;
	int getnum;
	BOOL m_lock;
	CCMenuItem *next;
	CCMenuItem *exit;
	BOOL m_unlock;
	BOOL m_unlock1_first;
	BOOL m_unlock2_first;
	BOOL m_unlock3_first;
	CCLabelAtlas *numLabel;//金币计数
	int m_index;
	CCSprite *p_bg2;
	BOOL falldone;
	
	CCSprite *blur1;
	CCSprite *blur2;
	CCSprite *blur3;

	
	
}
+(id) scene;
-(void)initSound;
- (id)initWithdata:(BOOL)endless:(int)player:(int)state:(int)Lastscore:(int)Lastnum:(int)LastHamburg;
- (void)loadNum;
- (void)saveNum;
- (void)step:(ccTime)dt;
- (void)touchtogo;
-(void)initItem;
@end
